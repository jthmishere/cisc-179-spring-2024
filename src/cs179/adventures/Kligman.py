#INTRO---------------------------------------------------------------------------------------------

print("You've been sent to a small town on a mission. You've been hired\n"
      "to assassinate one of the townsfolk! Your target was described to\n"
      "you as a 'burly man\'. Your employer has given you money for supplies.\n\n")

print("You have many options available to you! Explore the town and\n"
      "decide your course of action. When selecting an option, type 1, 2, 3 or 4.\n"
      "There are 5 special endings.\n\n")

#TEXT----------------------------------------------------------------------------------------------

townCenterChoices = ("You are in the town center.\n\n"
                     "1. Enter the bar\n2. Enter the shop\n3. Enter the palace\n4. Walk to the temple\n")

barChoices = ("You are in the bar. You spot the burly man in the corner!\n\n"
              "1. Attack the burly man\n2. Buy the burly man a drink"
              "\n3. Buy yourself a drink\n4. Back to town center\n")

shopChoices = ("You are in the shop.\n\n"
               "1. Buy \'Pint Of Intoxicant\'\n2. Buy gun"
               "\n3. Back to town center\n")

templeChoices = ("You are standing in front of the temple. There is a sprawling mass of \n"
                 "vegetation that covers the temple walls and the wet earth.\n\n"
                 "1. Enter the temple\n2. Ask monk about the plant"
                 "\n3. Pick some of the plant\n4. Back to town center\n")

inTempleChoices = ("As you enter the temple, all eyes fall on you.\n"
                   "Head Monk: \"We welcome you, stranger. If you wish to join our school of\n"
                   "thought, you must journey to the holy mountain alone. It is quite the\n"
                   "endeavor, but it is the only way.\n\n"
                   "1. Agree to become a monk\n2. Ask about the school of thought"
                   "\n3. Attack the monks\n4. Back to outside\n")

attack1 = ("You swing at the burly man with your bare hands...\n"
           "But it seems you've forgotten a key detail about your target.\n"
           "He is burly! He easily blocks your punch and pummels you into the ground.\n\n"
           "YOU DIED")

attack2 = ("You take aim and fire the gun at the burly man...\n"
           "The burly man dies and you are arrested immediately.\n"
           "The royal guards inform you that you will be excecuted tomorrow.\n\n"
           "Hopefully you have an escape plan...\n\n"
           "You got the \"Guns Blazing\" ending!")

giveDrink1 = ("The burly man thanks you for your generosity and guzzles\n"
              "the drink. Good thing your employer doesn't know that their\n"
              "target is drinking on their dime.\n")

giveDrink2 = ("You combine your \'Pint Of Intoxication\' with your \'Succulent Of Necrosis\'\n"
              "to form a POISON. It looks no different than a normal drink.\n"
              "The burly man thanks you for your generosity and guzzles\n"
              "the drink. You swiftly exit the bar as the burly man begins to colapse.\n"
              "By the time he dies you're long gone and claim your bounty.\n\n"
              "You got the \"Sneaky-Beaky-Like\" ending!")

drunk = ("The world starts spinning and you realize you've had one too many.\n"
         "With no inhibition, you climb onto a bar table and begin singing off\n"
         "key at the top of your lungs. Your unsteady feet knock other patrons\'\n"
         "drinks off the table, shattering on the ground. The bartender drags you\n"
         "by the collar to the curb outside where you promptly pass out.\n\n"
         "Upon waking up, you suddenly remember why you were sent here and rush\n"
         "back into the bar. But the burly man is nowhere to be found. Hungover and\n"
         "frantic, you decide to flee rather than return to your employer with bad news.\n\n"
         "You got the \"Drunk and Disorderly\" ending!")

runaway = ("You become indoctrinated into the temple. You set out on a solo voyage to the \n"
           "holy mountain. As you abandon your old life, you look back with a refreshed \n"
           "outlook on life.\n\n"
           "You got the \"Runaway\" ending!")

palaceChoices = ("Two royal guards block the main gate.\n"
                 "There is a drifter idling by a tree.\n"
                "You notice wanted posters everywhere.\n\n"
                "1. Attack the guards\n2. Attack the drifter"
                "\n3. Talk to the drifter\n4. Back to town center\n")

skyrim = ("As you attempt your first blow on the drifer he snaps his fingers with a coy\n"
              "smile. Magical sparks zoom from his fingers to your forehead. You fade into\n"
              "blackness. The blackness fades and you wake up in the back of a horsedrawn\n"
              "carraige, handcuffed. There are several others in handcuffs with you.\n"
              "The one on your left says: \"Hey, you, you're finally awake\"")

drifterChoices = ("The drifter gives you a coy smile...\n\n"
                  "1. Ask who he is\n2. Give\n3. Back to palace\n")

pacifist = ("The drifter quickly chugs the drink. He smiles brightly and begins to cackle.\n"
            "Rolling your eyes, you begin to walk away. The drifter taps your shoulder...\n\n"
            "You turn around and gasp. The drifter is levitating off the ground and glowing.\n"
            "The nearby guards don't seem to notice even though this is happening right in\n"
            "front of them. Smiling even wider, the drifter floats to you...\n\n"
            "Magic Drifter: \"I know what you have to do... There is a better way!\n"
            "I'll make that burly fellow look like him!\"\n"
            "The drifter gestures towards the wanted posters. You nod your head yes...\n\n"
            "The smoke clears and you walk out of town towards your employers location...\n"
            "As you walk you can see the burly man being led away by royal guards. Your\n"
            "hands feel clean.\n\n"
            "You got the \"Pacifist / Drifter Friend\" ending!")


#PLAYER STARTING SETUP------------------------------------------------------------------------------

playerPosition = "townCenter"

hasGun = False

hasPOI = False

hasSON = False

playerDrinks = 0

#ROOMS----------------------------------------------------------------------------------------------

def positionPicker (position):
    if position == "townCenter":
        townCenter()
    if position == "bar":
        bar()
    if position == "shop":
        shop()
    if position == "attack":
        attack()
    if position == "temple":
        temple()
    if position == "giveDrink":
        giveDrink()
    if position == "drink":
        drink()
    if position == "inTemple":
        inTemple()
    if position == "palace":
        palace()
    if position == "talkToDrifter":
        talkToDrifter()
        
def townCenter():
    print(townCenterChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        playerPosition = "bar"
        positionPicker(playerPosition)
    elif choice == 2:
        playerPosition = "shop"
        positionPicker(playerPosition)
    elif choice == 3:
        playerPosition = "palace"
        positionPicker(playerPosition)
    elif choice == 4:
        playerPosition = "temple"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        townCenter()

def bar():
    print (barChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        playerPosition = "attack"
        positionPicker(playerPosition)
    elif choice == 2:
        playerPosition = "giveDrink"
        positionPicker(playerPosition)
    elif choice == 3:
        playerPosition = "drink"
        positionPicker(playerPosition)
    elif choice == 4:
        playerPosition = "townCenter"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        bar()

def shop():
    print (shopChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        global hasPOI
        hasPOI = True
        print("Purchased.")
        shop()
    elif choice == 2:
        global hasGun
        hasGun = True
        print("Purchased.")
        shop()
    elif choice == 3:
        playerPosition = "townCenter"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        shop()

def temple():
    print (templeChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        playerPosition = "inTemple"
        positionPicker(playerPosition)
    elif choice == 2:
        print("You ask a nearby monk about the strange plant.\n"
              "Monk: \"Be careful. It's called \'Succulent Of Necrosis\'.\n"
              "I wouldn't eat it.\"\n\n")
        temple()
    elif choice == 3:
        global hasSON
        hasSON = True
        print("You picked a blade of the plant.\n")
        temple()
    elif choice == 4:
        playerPosition = "townCenter"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        temple()

def attack():
    if (hasGun == True):
        print(attack2)
    else:
        print(attack1)

def giveDrink():
    if (hasPOI and hasSON):
        print(giveDrink2)
    else:
        print(giveDrink1)
        bar()

def drink():
    global playerDrinks
    playerDrinks += 1
    print("You enjoy a nice cold brew on your employer's dime.\n\n")
    
    if (playerDrinks > 4):
        print(drunk)
    else:
        bar()

def inTemple():
    print(inTempleChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        print(runaway)
    elif choice == 2:
        print("Monk: \"You can become powerful. You can escape the squabbles of this world...\"\n\n")
        inTemple()
    elif choice == 3:
        print("You feel a blankness wash over you. You forgot what you were trying to do....\n\n")
        inTemple()
    elif choice == 4:
        playerPosition = "temple"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        inTemple()

def palace():
    print(palaceChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        print("The royal guards easily fend you off and retaliate, killing you instantly.\n"
              "YOU DIED")
    elif choice == 2:
        print(skyrim)
    elif choice == 3:
        playerPosition = "talkToDrifter"
        positionPicker(playerPosition)
    elif choice == 4:
        playerPosition = "townCenter"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        palace()

def talkToDrifter():
    print(drifterChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        print("Drifter: \"I'm thirsty. And magical. But mostly thirsty.\"\n\n")
        talkToDrifter()
    elif choice == 2:
        global hasPOI
        if hasPOI:
            print(pacifist)
        else:
            print("The drifter thanks you for the spare change. He gives you a coy smile.\n\n")
            talkToDrifter()
    elif choice == 3:
        playerPosition = "palace"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        talkToDrifter()

#START----------------------------------------------------------------------------------------------

positionPicker(playerPosition)


    




