import random
x = ""
correct = False
player_health = 20
skeleton_health = 25
player_inventory = [x]
# x = weapon
# y = weapon
###################################           Choose a Weapon         ##################################################
user_permission = input("\nHello Would You Like to Play Escape the Catacombs")

if user_permission != "Yes" and user_permission != "yes":
    print(" Thank you for Playing ")
    quit()






else:
    print(" You wake up, on the floor in front of you, you see a chest")
    print(
        " As open the chest, you see three weapons but you only have enough strengh to carry one. "
        "You must choose carefully:")
    weapon = input(" A.) Sword \n B.) Magic Staff \n C.) Bow and Arrow")

    if weapon == "A" or weapon == "a":
        x = player_inventory[0] = ("Sword")  # ("Sword") #1
    elif weapon == "B" or weapon == "b":
        x = player_inventory[0] = ("Magic Staff")  # ("Magic Staff") #2
    elif weapon == "C" or weapon == "c":
        x = player_inventory[0] = ("Bow and Arrow")  # ("Bow and Arrow") #3
    else:
        print("Please choose A,B or C")

    print("You have chosen a " + player_inventory[0])
    ###################################           Choose a Path           ##############################################
    print("\nAs you look around, You see a torch on the wall, you pull it off the wall "
          "and it begins to illuminate the room")
    print("\nAs you look around the room you see 3 entry ways")

    print(" North, a long hallway with a dark shadowy figure at the end\n"
          " East, you can see a chain gate with a small sleep goblin in front of it\n"
          " West, You can see a cobweb ladder that that goes up")

    direction = input("What direction would you like to go")

    # CHOOSE TO NORTH
    if direction == "North" or direction == "north":
        print("You have chosen the Northern hall")
        print("As you are walking, the shadowy figure goes through a wooden door and you follow behind \n"
              "As you walk past the door it slams behind you and an armored skeleton is in your way! ")

        boss = input("You are only have three choices \n A.) Fight" "\n B.Talk")
        # CHOOSE TO FIGHT
        if boss == "A" or "a":
            print("You chose to Fight")
            if player_inventory[0] == "Sword":
                print("Run forward and swing with all your might. ")
                print("You land a slice him in half, snapping all his bone in half")
                print("As he lays defenseless and sad, You grab the key off his neck and search for a door")

            elif player_inventory[0] == "Magic Staff":
                print("Step back and cast a spell")
                print("You blast him with fire and burn him to a crisp, leaving a golden key in the heap."
                      "As you grab the key, you search for a door")
                print("You grab the key and search for a door")

            elif player_inventory[0] == "Bow and Arrow":
                print("Arch you bow and shoot aiming for the head")
                print("You land a headshot and knock his head off, as he searches for his head he trips and"
                      " you see a bright golden key fall from his pocket ")
                print("You grab the key and search for a door")

        # CHOOSE TO TALK
        elif boss == "B" or "b":
            print("You chose to talk")
            talk = input("You are only have three choices "
                         "\n A.) Start barking"
                         "\n B.Ask him what he want (Aggressively )"
                         "\n C.)Ask if he can help you leave this creepy place(nicely)")

            if talk == "A" or "a":
                print("The Skeleton remembers his traumatized childhood and Runs off leaving a key behind ")
            elif talk == "B" or "b":
                print("He states he wants you dead, and proceeds to pull out his sword"
                      "\nYou have no choice but to fight")


            elif talk == "C" or "c":
                print("He stares at you confused and states,'There is no way out why not just die by his hand to save "
                      "time',\nYou have no choice but to fight")

    # CHOOSE TO EAST
    elif direction == "East" or direction == "east":
        print("You have chosen the Eastern gate\n")
        print("As you get closer you see that the sleeping goblin has a key tied to his belt")
        choice1 = input("\n You have 3 choices \nA.) Sneak up on him and try to steal the key"
                        "\n B.) Wake up the Goblin and ask him for the key "
                        "\n C.)Aim your weapon at him and demand he gives you the keys")

        if choice1 == "A" or "a":
            print("He wakes us as you are over him and quickly slaps your hand away")
            print("Angry that you tried to steal from him the goblin pulls out a dagger")
            print("Key ")
        elif choice1 == "B" or "b":
            print("He grumbles for a bit but eventually wakes up and in a half daze gives you the key")
        elif choice1 == "C" or "c":
            print("He wakes up confused and scared and pulls out a dagger!"
                  "He demands to know who you are and why you are attacking him"
                  "You tell him to hand over the key or you brutally attack him ")
            print("Nervously he hands over the key "
                  "And you begin looking for a door")

    # CHOOSE TO WEST
    elif direction == "West" or direction == "west":  # west:
        print("You have chosen the Western ladder\n")
        print("As you approach the ladder you start to here whispering "
              "As you start climbing down the hatch above closes and a Giant Human-size Spider climbs acrosses it "
              "trapping you in the room")
        print("As you let go of the ladder you land on you feet on the floor, with cobwebs covering the floor you "
              "are unable to move")

    print("After getting the key you found a door with no lock but a small statue with etching in the side")
    print(" You read that it says 'You must solve this riddle to pass' You have 5 guesses")


    answer = input("What goes up, but never comes down? ")
    while (answer != "man"):
        print("Try again")
        correct = True
        answer = input("What goes on four legs in the morning, two legs in the afternoon, and three legs in the evening?? ")

if correct == True:
        print("The door open and you see a long hallway with a light at the end"
              "YOU HAVE ESCAPES CONGRATS")
