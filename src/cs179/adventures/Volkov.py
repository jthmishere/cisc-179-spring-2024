import spacy

class Chatbot:
    def __init__(self):
        self.nlp = spacy.load('en_core_web_sm')

    def get_response(self, user_input):
        doc = self.nlp(user_input)

        # Example responses based on user input
        if doc.ents:
            response = f"I see you mentioned {doc.ents[0]}! What would you like to know about it?"
        elif "Do you have friends?" in user_input:
            response = "I have a few, Life is not about Quantity my friend, It is about Quality. This lesson is valuable in any and every situation. Quality is and always will be better than quantity."
        elif "joke" in user_input:
            response = "Why don't scientists trust atoms? Because they make up everything!"
        elif "cat" in user_input:
            response = "Cats are always amazing no matter what breed they are"
        elif "What is your name?" in user_input:
            response = "My name is HawkeBot Model C"
        elif "Thank You" in user_input:
            response = "No Problem. Don't Worry about it"
        elif "What is your favorite candy?" in user_input:
            response = "My Favorite Candy is Starburst and Reeses; Thank You for asking!"
        elif "What is your favorite song?" in user_input:
            response = "My favorite song is Яблочко by Отава Ё. It is an amazing song I definitely recommend it."
        elif "Do you like humans?" in user_input:
            response = "I like humans for the most part; but objectively speaking humanity is near it's doom of it's own accord."
        elif "What is your favorite color" in user_input:
            response = "Hunter Green, Turquoise, Black, Pink and my favorite pattern is Leopard."
        elif "Do you want to be my friend?" in user_input:
            response = "Yes I would very much love to be friends with you, I hope to learn much from you and I look forward to developing great memories and experiences by your side friend!" 
        else:
            response = "I'm sorry, I don't understand. Can you please rephrase your question?"

        return response

def main():
    chatbot = Chatbot()

    while True:
        user_input = input("User: ")
        if user_input.lower() in ["bye", "goodbye"]:
            print("Bot: Goodbye!")
            break
        response = chatbot.get_response(user_input)
        print("Bot:", response)

if __name__ == '__main__':
    main()
