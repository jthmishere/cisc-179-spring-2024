print("Welcome space explorer to Elysium! What is your name?")
name = input(" Enter Your Name ")
print(f'How would you like to proceed {name}?')
start = input("Would you like to board the train into Elysium? Yes or No ")

while start.lower() not in ['yes', 'no']:
    print(f'Its a Yes or No answer {name} ...')
    start = input("Would you like to board the train into Elysium? Yes or No")

if start.lower() == 'yes':
    print("The train door closes behind you and speed off towards the city!")
elif start.lower() == 'no':
    print("You sadly return to the space station but are soon kidnapped into deep space! Goodbye forever...")
    quit()
setting = input("Welcome to Elysium the Citadel of the Outer Rim. Want to visit the 'bar' or the 'security' station? ")

if setting.lower() in ['security', 'security station']:
    print(f'{name} enters the security station and is greeted by the Elysium security chief.')
    print("You mention to the chief you're looking for a missing girl.")
    print("'The chief thinks for a moment and says, 'I remember seeing a suspicious individual in the area around the mayor's office. Maybe you should check it out.'")
    direction = input("Do you 'go' to the mayor's office or 'stay' at the security station? ")
    while direction.lower() not in ['go', 'stay']:
        print(f'Please enter a valid option, {name}...')
        direction = input("Do you 'go' to the mayor's office or 'stay' at the security station? ")
    if direction.lower() == 'go':
        print("You thank the chief and head towards the mayor's office.")
        print("You arrive at the mayor's office and are greeted by a secretary.")
        print("You tell the secretary about the missing girl and she tells you that the mayor might have some information.")
        print("The secretary points you to the mayor's office at the end of the hallway.")
        direction = input("Which way do you go? 'left' or 'right'?")
        if direction.lower() == 'left':
            print("You take a left and down the hallway, you hear a meeting going on in one of the offices.")
            action = input("Do you 'bust in' or 'listen in'?")
            if action.lower() == 'bust in':
                print("You kick open the door and you see the mayor with a man in a mask.")
                print("Before you can react you get choked from behind and pass out.")
                quit()

            elif action.lower() == 'listen in':
                print("You press your ear against the door and listen carefully.")
                print("You hear mentions of kidnapped girls being held at a storage warehouse.")
                print("You go ahead and race towards the storage warehouse in hopes of finding her.")
                location = 'storage warehouse'
                print("You show up at the warehouse and sneak in from the roof of the warehouse.")
                print("You search the warehouse and find the missing girl tied up in a corner.")
                print("You untie her and bring her to safety. Before you leave you notice three figures gather the mayor, the security chief and the masked individual. ")
                expose = input("Who will you report to the news 'the mayor', 'the security chief', or 'the masked individual'. ")
                if expose.lower() == 'the mayor':
                    print("Mayor was imprison for corruption running a kidnapping ring and his whole operation was shut down. ")
                    quit()
                elif expose.lower() == 'the security chief':
                    print("The security chief claims his innocence and awaits trail. He states I was there on official business and received a tip about a kidnapping ring.")
                    quit()
                if expose.lower() == 'the masked individual':
                    print("Masked individual was reported was never found or thought to have never existed.")
                    quit()

    elif direction.lower() == 'stay':
        print("You decide to stay at the security station and look for more clues.")
        print("As you look around a masked man visit the station and meets with the security chief")
        print("You take that chance to snoop into the security chief office.")
        print("You notice on his desk a map with a storage warehouse marked, along with a pistol and a security clearance badge.")
        take = input("Footsteps are quickly approaching you have a chance to take either the 'gun' or 'badge'. What do you take?")
        if take.lower() in ['gun', 'badge']:
            print(f"You quickly swipe the {take} and leave the back of the station heading to the storage warehouse.")
            location = 'storage warehouse'
            print("You arrive at the warehouse thanks to the map you found the back entrance with only one guard. ")
            print(f"You use the {take} to confront the guard and he lets you pass.")
            print("You open the back room and you notice two rooms.")
            print("You notice two rooms a vault and cries for help in the back office.")
            finalv1 = input("Do you go to the 'vault' or 'back office'.")
            if finalv1.lower() == 'vault':
                print("You enter the vault money bags full are laying around.")
                print("You go to grab as many bags as you can when you turn around and the security chief has you hold at gun point")
                print("Security chief calls you a noisy little dog and fires three shots you before you can talk. ")
                import time
                text = "You are dead.."
                for letter in text:
                    print(letter, end='', flush=True)
                time.sleep(0.5)
                quit()
            elif finalv1.lower() == 'back office':
                print("You enter the back offices and you finally found her.")
                print("You rush over to her and untie the ropes. She looks scared and confused, but grateful to see you.")
                print("You reassure her that everything's going to be okay and lead her out of the warehouse.")
                print("You make your escape and return her home safe and sound.")
                quit()


elif setting.lower() in ['bar', 'the bar']:
    print("You enter the bar. The lights are dimmed you grab a seat right at the bar.")
    decide = input("Order a 'drink' or 'ask' the bartender about a missing girl? ")
    if decide.lower() in ['order a drink', 'drink']:
        print("You down the drink and start to feel woozy.")
        drink = input("Do you drink more or confront bartender? ")
        if drink.lower() in ['drink', 'drink more']:
            print("You drink more and more then blackout...")
            quit()
        elif drink.lower() in ['confront', 'confront bartender']:
            print("You grab the bartender and he slams a bottle on your head. You wake up in the security station with a throbbing headache.")
            print("The security chief tells you cause enough trouble and plans to deport you from Elysium...")
            quit()

    elif decide.lower() in ['ask', 'ask the bartender']:
        print("Bartender says he saw a little girl with a shady figure. A mask figure watches you in the corner.")
        buy = input("Do you 'buy' him a drink or 'chase' him? ")
        if buy.lower() in ['buy', 'buy a drink']:
            print("You buy him a drink and he smiles. Let's make a deal I'll give you info about the girl")
            print("He provides you with the info you need and you head to the storage warehouse.")
            print("The place is abandon but you found the girl as you untie her and set you free you notice the masked indvidual.")
            print("He's stealing multiple bags of money in the vault and sets off the alarm.")
            stop = input("Do you 'leave' free with the girl or 'stop' the masked individual.")
            if stop.lower == 'leave':
                print("You let him get away with the girl in hand you are quickly surrounded by officers and are arrested for suspected robbery.")
                quit()
            elif stop.lower == 'stop':
                print("You run at him full speed and tackle him with him in hand you delivery him to the cops and notice the girl ran off.")
                print("You are treated as a hero but the girl you've been searching for is forever lost.")
                quit()


        elif buy.lower() in ['chase', 'chase a man']:
            print("You chase the man in the mask and it leads you to a storage warehouse.")
            print("You chase the man and find yourself quickly surronded by thugs where they go on to beat the life out of you. ")

        
            quit()














