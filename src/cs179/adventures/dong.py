print("In a far away kingdom, the king has requested you, his knight, to recover his stolen treasure from some thieves.")
print("You reluctantly agree as you don't have a choice. You pack your bags and quickly set off alone to recover the treasure.")
print("As you begin you journey, you encounter a giant snail blocking your path.")
print("You try going around it, but the snail doesn't allow you to. You realize you have two options.")
print("You can either attack the snail to get it to move, or you can lead it off the path with some lettuce.")
choice = input("(attack/lettuce) -> ").lower().strip()
if choice == "attack":
    print("\nYou grasp your mighty sword and swing at the giant snail. The snail screams in pain and collapses on the side of the path. ")
    print("The path is cleared. You continue on your journey until you reach a river and stop to rest.")
    print("While you take a drink from the river, a dog steals you satchel and runs off. You follow it to see it run into a cave.")
    print("You chase it into the cave until a large cage tied together by branches and vines falls trapping you inside of it.")
    choice = input("(escape/stay) -> ").lower().strip()
    if choice == "escape":
        print("\nYou quickly break through the cage before anyone sees you. But as you are about to escape, your capturer, who you realize is the thief sees you.")
        print("Do you run or surrender?")
        choice = input("(surrender/run) -> ").lower().strip()
        if choice == "surrender":
            print("\nYou stop and put you hands up surrendering to the thief. The thief bounds you hand together.")
            print("He realizes you are a knight. He quickly reaches for his knife and swings at you but in that moment, you put your hands up and he cuts your hands free.")
            print("You quickly run to get away from the thief. But you stop and see the stolen treasure.")
            print("Do you attack the thief or grab the treasure and run?")
            choice = input("(attack/run) -> ").lower().strip()
            if choice == "attack":
                print("\nYou tackle the thief to the ground and subdue him. Do you kill him?.")
                choice = input("(yes/no) -> ").lower().strip()
                if choice == "yes":
                    print("\nYou look over and see the thiefs knife lying beside you. You grab it and implant it into his chest killing him.")
                    print("You stand up and grab the king's treasure and return to his castle.")
                elif choice == "no":
                    print("\nYou stand up and grab the king's treasure to return to the castle.")
                    print("As you are leaving the cave, the thief runs at you and stabs you with his knife. You died.")
                    
            elif choice == "run":
                print("\nYou run to the exit of the cave but you see the treasure. Do you grab it?")
                choice = input("(grab/run) -> ").lower().strip()
                if choice == "grab":
                    print("\nYou try to grab the treasure by the thief blocks you and stabs you with his knife. You Die.")
                elif choice == "run":
                    print("\nYou run out of the cave and return to the king empty handed.")

        elif choice == "run":
            print("\nYou run out of the cave without getting injured. Do you return to the castle or go back into the cave to get the treasure?")
            choice = input("(cave/castle) -> ").lower().strip()
            if choice == "cave":
                print("\nYou quickly get yourself together and reenter the cave. You see the thief and the treasure.")
                print("Who do you pursue?")
                choice = input("(thief/treasure) -> ").lower().strip()
                if choice == "thief":
                    print("\nYou attack the thief but find yourself weaponless. The thief easily kills you. You Die.")
                elif choice == "treasure":
                    print("\nYou quickly run and grab the treasure. After you bolt out of the cave and return to the king with the treasure in hand.")
            elif choice == "castle":
                print("\nAs you are returning to the castle, you know the king will be angry at you. Do you still return to the castle or run away?")
                choice = input("(castle/run) -> ").lower().strip()
                if choice == "castle":
                    print("\nYou return to the castle without the king's treasure. The king is angry and disapointed in you. He sentences you to death. You Die.")
                elif choice == "run":
                    print("\nYou decide to run away from the castle. You end up living a long life far from trouble.")
    elif choice == "stay":
        print("\nYou stay put inside of the cage until your capturer, who you realize is the thief appears.")
        print("He asks you who you are. How do you respond? Do you confess you are a knight or say you are a traveller?")
        choice = input("(knight/traveller) -> ").lower().strip()
        if choice == "knight":
            print("\nYou reveal yourself to the thief. The thief is surprised that you tell him this information.")
            print("The thief then asks why you are here. Do you tell him the truth?")
            choice = input("(truth/lie) -> ").lower().strip()
            if choice == "truth":
                print("\nYou tell the thief that you are here from the request of the king to retrieve the treasure.")
                print("The thief is once again surprised that you tell him this. He is so surpried that he offers you to join is side to overthrow the king.")
                print("Do you accept this offer?")
                choice = input("(accept/deny) -> ").lower().strip()
                if choice == "accept":
                    print("\nYou accept the thief's offer to overthrow the king. The thief releases you and soon continue to overthrow the king.")
                elif choice == "deny":
                    print("\nYou deny the thief's offer to overthrow the king. The thief is understanding to your loyalty to the king.")
                    print("Becuase of this the thief has no option but to kill you. The thief picks up his knife implants it into your chest. You Die.")
            elif choice == "lie":
                print("\nYou tell the thief that you are here looking for an escaped prisioner. The thief is intrested in what you said.")
                print("But since the thief knows you are a knight, he has to kill you. He grabs his knife and opens the cage to kill you.")
                print("He swings his knife at you but in that split second, you are able to dodge his. Do you go right or left?")
                choice = input("(right/left) -> ").lower().strip()
                if choice == "right":
                    print("\nYou quickly dodge to the right. Your intution is right and you dodge his attack. You quickly tackle and subdue the thief.")
                    print("After you have delt with the thief, you grab the stolen treasure and return it to the king.")
                elif choice == "left":
                    print("\nYou quickly dodge to the left. You uncessfully avoid the knife and it is implanted into you chest. You Die.")
        elif choice == "traveller":
            print("\nYou tell the thief that you are a traveller form far away. The thief is intrested in this and frees you from the cage.")
            print("He offers you to sit by his campfire and you do. While you are chatting to him, you see the stolen treasure in the corner of your eye.")
            print("He catches you taking a glimse of the treasure. at this moment, you pull your sword out and hold it against him.")
            print("The thief is shocked. Do you reveal yourself now")
            choice = input("(yes/no) -> ").lower().strip()
            if choice == "yes":
                print("\nYou tell the thief that you are a knight that is here to kill him and retrieve the stolen treasure.")
                print("Right as you are making you killing blow, he shouts STOP. You stop and he tells you that if you spare him he will split you half the treasure.")
                print("Do you still kill the thief?")
                choice = input("(kill/spare) -> ").lower().strip()
                if choice == "kill":
                    print("\nYou kill the thief and return to the king with the stolen treasure.")
                elif choice == "spare":
                    print("\nYou agree to spare the thief and grab half the treasure. You walk out of the cave and don't return to the castle.")
            elif choice == "no":
                print("\nYou keep yourself hidden from the thief. The thief begs you for his life. He says that he stole the treasure from the king.")
                print("He adds that he only did it because he needed for his family. Do you kill him?")
                choice = input("(yes/no) -> ").lower().strip()
                if choice == "yes":
                    print("\nYou kill the thief and return to the kingdom with the treasure.")
                elif choice == "no":
                    print("\nYou spare the thief and leave with the treasure to the castle.")

elif choice == "lettuce":
    print("\nYou turn around and walk until you reach nice meadow with a garden in the middle. You pass by all types of vegetables until you see some lettuce.")
    print("You bend over to pick it unitl a group of large rabbits jump out and point spears at you. They confront you and ask for a price for the lettuce. ")
    print("To get the lettuce, the rabbits ask you to tell them an eight letter word")
    choice = input("(Input eight letter word) -> ").lower().strip()
    choice = len(choice)
    if choice == 8:
        print("\nYou are correct. The rabbits give you some lettuce. You return to the giant snail and feed it to him and moves out the way.")
        print("You continue on your journey until you reach a stranger in need of help. Do you help them?")
        choice = input("(help/ignore) -> ").lower().strip()
        if choice == "help":
            print("\nYou go and help the stranger. In return he wants to join you in your journey. Do you accept this offer?")
            choice = input("(accept/deny) -> ").lower().strip()
            if choice == "accept":
                print("\nThe stranger joins you on this journey. After a while walking, you are ambushed by the thief. The thief grabs the stranger.")
                print("Do you listen to the thief and put down you weapon or attack the thief?)")
                choice = input("(listen/attack) -> ").lower().strip()
                if choice == "listen":
                    print("\nYou put down you weapon and releases the stranger. The thief also takes all of your supplies leaving you stranded.")
                elif choice == "attack":
                    print("\nYou swing at the thief killing both the stranger and the thief. You search the thief's body and find the stolen treasure.")
                    print("You take the treasure and return to the castle.")
            elif choice == "deny":
                print("\nYou deny the help from the stranger and continue on your journey alone. You are quickly ambushed by the a thief who you realize stole the treasure.")
                print("Do you pull you sword or surrender?")
                choice = input("(attack/surrender) -> ").lower().strip()
                if choice == "attack":
                    print("\nYou pull your sword but you are too slow. The thief stabs you with his knife. You Die.")
                elif choice == "surrender":
                    print("\nYou drop your sword and put your hands up. The thief loots you and runs away leaving you stranded.")
        elif choice == "ignore":
            print("\nYou walk by the stranger without helping. As you continue on your journey, the thief pops out of a bush.")
            print("You see the thief has the stolen treasure and confront him. The thief is feeling generous today and asks you to play a game.")
            print("If you win he will let you go free. You accept to play the game.")
            choice = input("Insert a word with the letter 'q' in it: ").lower().strip()
            if "q" in choice:
                    print("\nYou Win! The thief gives you the treasure.")
            else:
                print("\nYou Loose! The thief kills you.")
    elif choice != 8:
        print("\nThat is incorrect. Now you must die!!!")
        choice = input("(attack/run) -> ").lower().strip()
        if choice == "attack":
            print("\nAs the rabbits rush after you, you pull your sword and slay all of the rabbits.")
            print("You go over and pull the lettuce from its roots and return to the giant snail. It accepts your offer and moves.")
            print("As you continue on your journey, you realize it is getting dark out and start looking for shelter.")
            print("You see a cabin in the middle of the woods. Do you say there to rest?")
            choice = input("(yes/no) -> ").lower().strip()
            if choice == "yes":
                print("\nYou enter the cabin. As find a place to rest on the floor, you hear the laugh of a witch.")
                print("The witch ask you why you are here and you tell her you are looking for stolen treasure.")
                print("The witch wants to play a game. She wants to play ""Rock, Paper, Scissors"". You agree as it is your only choice.")
                print("If you win the witch will summon your stolen treasure, if you loose or get a tie, you will pay the ultimate price.")
                import random

                your_choice = input("(rock/paper/scissors) -> ").lower().strip()
                your_choice = your_choice.lower().strip()
                rockpaperscissor = ("rock", "paper", "scissors")
                witch_choice = random.choice(rockpaperscissor)
                print("\n")
                print("You chose " + your_choice + "\nThe witch chose " + witch_choice + "\n")

                if your_choice == witch_choice:
                    print("It's a tie! YOU DIE")  # tie, loop, and print that it repeats
                elif your_choice == "rock":
                    if witch_choice == "scissors":
                        print("You win! The witch summons the stolen treasure and you return to the castle.")
                    else:
                        print("YOU DIE")
                elif your_choice == "paper":
                    if witch_choice == "rock":
                        print("You win! The witch summons the stolen treasure and you return to the castle.")
                    else:
                            print("YOU DIE")
                elif your_choice == "scissors":
                    if witch_choice == "paper":
                        print("You win! The witch summons the stolen treasure and you return to the castle.")
                    else:
                        print("YOU DIE")
                else:
                    print("Your answer is invalid. YOU DIE")

            elif choice == "no":
                print("\nYou walk past the cabin into the night. As it is getting dark, a bear jumps out and mauls you.")
                print("Do you grab your sword or try to sneak out under it.")
                choice = input("(sword/sneak) -> ").lower().strip()
                if choice == "sword":
                    print("\nYou try to grab your sword but it is too late. You Die")
                elif choice == "sneak":
                    print("\nYou try to sneak out under it but the weight of the bear is to heavy. You Die")
        elif choice == "run":
            print("\nYou run from the rabbits. But you are too slow and get captured. The rabbits give you a chance at life or death.")
            print("What do you choose?")
            choice = input("(life/death) -> ").lower().strip()
            if choice == "life":
                print("\nThe rabbits tie you up on a pole as their new scarecrow. You beg for forgiveness but they don't listen.")
                print("As time passes, your opportunity to escape has come. Do you escape or stay?")
                choice = input("(escape/stay) -> ").lower().strip()
                if choice == "escape":
                    print("\nYou escape from your torment and wreak havoc on the rabbit farm and now become a rabbit farmer.")
                elif choice == "stay":
                    print("\nYou spend the rest of your days as a scarecrow.")
            elif choice == "death":
                print("\nThe rabbits approach you ready to kill you. You see a gardening tool and hole in the ground.")
                print("Do you reach for the tool or jump in the hole?")
                choice = input("(tool/hole) -> ").lower().strip()
                if choice == "tool":
                    print("\nYou reach for the tool but you are too slow. You Die")
                elif choice == "hole":
                    print("\nYou jump in the hole only to see more rabbits. You Die")
else:
    print("Not Possible")
