
# _Data driven_ text adventures

The only new requirements are that your adventure has 6 room (instead of 5) and that it be **data driven**:

* Has 6 or more "rooms" (also called _states_)
* Has descriptions for all your rooms in a separate data file (`your_last_name.txt`)
* Understands 4 or more commands (also called _keywords_)
* Allows the user to visit all rooms
* Allows the user to reach the end of your data-driven text adventure

You should remember how to design and start "iterating" on your code (modifying it to make it change what it does).
Once you're done, you will just need to submit your code **AND** data files to this GitLab repository:
https://gitlab.com/tangibleai/community/cs179/-/tree/main/src/cs179/data_driven

### 8. Share you program **AND** data

0. Save your Python code file ("py file") using your last name as the file name. Mine would be "lane.py"
1. Save your data (room descriptions) to a text file using your last name again. Mine would be "lane.txt"
2. Upload your `lane.py` file to the `cs179/data_driven/` directory by clicking the plus sign [here](https://gitlab.com/tangibleai/community/cs179/-/tree/main/src/cs179/data_driven)
3. Click _upload_ to browse your file system and open the file on your computer to upload it to GitLab
4. Hit the blue "Commit changes" button at the bottom of the page (this saves your file on GitLab)
5. Click the blue "Create merge request" button.
6. On the next page click the blue "Create merge request" button at the bottom of the page again.
7. Upload your `lane.txt` file to the `cs179/data_driven/` directory by clicking the plus sign again [here](https://gitlab.com/tangibleai/community/cs179/-/tree/main/src/cs179/data_driven) and repeating steps 3-6 above.

## References

* [`gitlab.com/tangibleai/community/cs179/-/tree/main/src/cs179/adventures`](https://gitlab.com/tangibleai/community/cs179/-/tree/main/src/cs179/adventures)
* [`gitlab.com/tangibleai/community/cs179/-/tree/main/src/cs179/data_driven`](https://gitlab.com/tangibleai/community/cs179/-/tree/main/src/cs179/data_driven)
