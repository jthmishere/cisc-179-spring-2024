with open("pham.txt", "r") as f:
    game_text = f.readlines()

game_text = [s.strip() for s in game_text]

print(game_text[0])
name = input("Enter Your Name ")
print(game_text[1].format(name))
print(game_text[2])
print(game_text[3].format(name))
start = input(game_text[4])
while start.lower() not in ['yes', 'no']:
    print(f"It's a yes or no answer, {name}...")
    start = input(game_text[4])

if start.lower() == 'yes':
    print(game_text[5])
    setting = input(game_text[6])
    while setting.lower() not in ['security', 'medical']:
        print(f'Not valid, Security or Medical')
        setting = input(game_text[6])

    if setting.lower() == "security":
        print(game_text[7].format(name))
        print(game_text[8])
        print(game_text[9])
        investigate = input(game_text[10])
        while investigate.lower() not in ['mayor','chief']:
            print(f'Not valid, Mayor or Chief')
            investigate = input(game_text[10])
        if investigate.lower() == "mayor":
            print(game_text[11])
            print(game_text[12])
            print(game_text[13])
            print(game_text[14])
            action = input(game_text[15])
            while action.lower() not in ['listen in', 'bust in']:
                print(f'Not valid, Listen in or Bust in')
                action = input(game_text[15])
            if action.lower() == "listen in":
                print(game_text[16])
                print(game_text[17])
                print(game_text[18])
                print(game_text[19])
                print(game_text[20])
                print(game_text[21])
                quit()
            elif action.lower() == "bust in":
                print(game_text[22])
                print(game_text[23])
                quit()
        elif investigate.lower() == "chief":
            print(game_text[24])
            print(game_text[25])
            print(game_text[26])
            print(game_text[27])
            take = input(game_text[28])
            while take.lower() not in ['gun', 'badge']:
                print(f'Not valid, Gun or Badge')
                take = input(game_text[28])
            if take.lower() == "gun":
                print(game_text[28])
                print(game_text[29])
                print(game_text[30])
                print(game_text[31])
                print(game_text[32])
                print(game_text[33])
                quit()
            elif take.lower() == "badge":
                print(game_text[34])
                print(game_text[35])
                print(game_text[36])
                swipe = input(game_text[37])
                while swipe.lower() not in ['vault', 'holding cell']:
                    print(f'Not valid, Vault or Holding cell')
                    swipe = input(game_text[37])
                if swipe.lower() == "vault":
                    print(game_text[38])
                    print(game_text[39])
                    print(game_text[40])
                    quit()
                elif swipe.lower() == "holding cell":
                    print(game_text[41])
                    print(game_text[42])
                    quit()

    elif setting.lower() == "medical":
        print(game_text[43])
        decide = input(game_text[44])
        while decide.lower() not in ['bar', 'report']:
            print(f'Not valid, Bar or Report')
            decide = input(game_text[44])
        if decide.lower() == "bar":
            print(game_text[45])
            order = input(game_text[46])
            while order.lower() not in ['drink', 'ask']:
                print(f'Not valid, Drink or Ask')
                order = input(game_text[46])
            if order.lower() == "drink":
                print(game_text[47])
                drink = input(game_text[48])
                while drink.lower() not in ['drink', 'confront']:
                    print(f'Not valid, Drink or Confront')
                    drink = input(game_text[48])
                if drink.lower() == "drink":
                    print(game_text[49])
                    quit()
                elif drink.lower() == "confront":
                    print(game_text[50])
                    print(game_text[51])
                    quit()
            elif order.lower() == "ask":
                print(game_text[52])
                buy = input(game_text[53])
                while buy.lower() not in ['buy', 'chase']:
                    print(f'Not valid, Buy or Chase')
                    buy = input(game_text[53])
                if buy.lower() == "buy":
                    print(game_text[54])
                    print(game_text[55])
                    offer = input(game_text[56])
                    while offer.lower() not in ['offer', 'rescue']:
                        print(f'Not valid, Offer or Rescue')
                        offer = input(game_text[56])
                    if offer.lower() == "offer":
                        print(game_text[57])
                        print(game_text[58])
                        quit()
                    elif offer.lower() == "rescue":
                        print(game_text[59])
                        print(game_text[60])
                        quit()
                elif buy.lower() == "chase":
                    print(game_text[61])
                    print(game_text[62])
                    quit()
        elif decide.lower() == "report":
            print(game_text[63])
            print(game_text[64])
            quit()

elif start.lower() == 'no':
    print(game_text[65])
    quit()













